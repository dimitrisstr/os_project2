#include "malloc.h"


void* emalloc(int size) {
    void *m = malloc(size);
    if (m == NULL) {
        fprintf(stderr, "out of memory\n");
        exit(EXIT_FAILURE);
    }
    return m;
}