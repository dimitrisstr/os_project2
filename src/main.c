#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include "parse.h"

#define OFFSET 0
#define POINTS_TO_READ 1


int **calculateWorkersData(FILE *input_binary_file, int workers_count);


int main(int argc, char *argv[]) {
    if (argc < 7) {
        printf("usage: %s -i InputBinaryFile -w "\
                         "WorkersCount -d TempDir\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    int **workers_data, workers_count = 0;
    char *temp_directory = NULL, *input_binary_file_path = NULL;
    FILE *input_binary_file = NULL;

    /*
     * Read arguments
     */
    int i;
    for (i = 1; i < argc - 1; i++) {
        if (strcmp(argv[i], "-i") == 0) {       //input binary file
            input_binary_file = fopen(argv[i + 1], "rb");
            if (input_binary_file == NULL) {
                perror("input binary file");
                exit(EXIT_FAILURE);
            }
            input_binary_file_path = argv[i + 1];
        }
        else if (strcmp(argv[i], "-d") == 0) {  //temp directory
            //create directory if it doesn't exist
            struct stat st;
            if (stat(argv[i + 1], &st) == -1)
                mkdir(argv[i + 1], 0700);
            temp_directory = argv[i + 1];
        }
        else if (strcmp(argv[i], "-w") == 0)    //number of workers
            workers_count = atoi(argv[i + 1]);
    }

    if (workers_count <= 0) {
        fprintf(stderr, "Invalid number of workers!\n");
        exit(EXIT_FAILURE);
    }

    workers_data = calculateWorkersData(input_binary_file, workers_count);
    fclose(input_binary_file);

    startCLI(input_binary_file_path, workers_data, workers_count, temp_directory);

    return EXIT_SUCCESS;
}


int **calculateWorkersData(FILE *input_binary_file, int workers_count) {

    int **workers, i;
    //create 2 x workers_count array
    workers = emalloc(sizeof(int*) * 2);
    for (i = 0; i < 2; i++)
        workers[i] = emalloc(sizeof(int) * workers_count);

    //find file's size
    fseek(input_binary_file, 0, SEEK_END);
    long int size = ftell(input_binary_file);
    rewind(input_binary_file);
    //number of records / size of record
    int records = size / (sizeof(float) * 2);
    //calculate number of points
    int worker_points = records / workers_count;
    int remaining_points = records % workers_count;
    int offset = 0;

    for (i = 0; i < workers_count; i++) {
        workers[POINTS_TO_READ][i] = worker_points + ( (remaining_points--) > 0);
        workers[OFFSET][i] = offset;
        offset += workers[POINTS_TO_READ][i];
    }
    return workers;
}
