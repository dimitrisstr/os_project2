#ifndef _CIRCLE_H
#define _CIRCLE_H

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "../point.h"

typedef struct Point* PointPtr;
typedef struct Circle* CirclePtr;

CirclePtr Circle_create(PointPtr center, int r);

void Circle_destroy(CirclePtr *circle);

int Circle_isInside(void *circle, PointPtr point);

#endif
