#include "circle.h"

typedef struct Circle {
    PointPtr center;    
    float r_squared;
} Circle;


CirclePtr Circle_create(PointPtr center, int r) {
    CirclePtr new_circle = malloc(sizeof(Circle));
    if (new_circle == NULL)
        return NULL;
    else {
        new_circle->center = center;        
        new_circle->r_squared = r * r;
        return new_circle;
    }
}


void Circle_destroy(CirclePtr *circle) {
    Point_destroy(&(*circle)->center);
    free(*circle);
    *circle = NULL;
}


int Circle_isInside(void *circ, PointPtr point) {
    /*
     * (pointX - centerX)^2 + (pointY - centerY)^2 <= r^2
     */
    CirclePtr circle = circ;
    float centerX = Point_getX(circle->center);
    float centerY = Point_getY(circle->center);
    float pointX = Point_getX(point), pointY = Point_getY(point);
    return  ( pow(pointX - centerX, 2.0) + pow(pointY -
                            centerY, 2.0) ) <= circle->r_squared;
}
