#ifndef _POINT_H
#define _POINT_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct Point* PointPtr;

PointPtr Point_create(float x, float y);

void Point_destroy(PointPtr *point);

float Point_getX(PointPtr point);

float Point_getY(PointPtr point);

void checkPoints(void *shape, int offset, int points_to_read_count,
    FILE *input_file, FILE *output_file, int (*isInside)(void*, PointPtr));

void Point_read(PointPtr point, FILE *input_file);

void Point_print(PointPtr point, FILE *output_file);

#endif
