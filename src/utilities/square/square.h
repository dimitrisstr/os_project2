#ifndef _SQUARE_H
#define _SQUARE_H

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "../point.h"

typedef struct Square* SquarePtr;

SquarePtr Square_create(PointPtr center, int r);

void Square_destroy(SquarePtr *square);

int Square_isInside(void *square, PointPtr point);

#endif
