#include "square.h"

typedef struct Square {
    PointPtr center;
    float r;
} Square;


SquarePtr Square_create(PointPtr center, int r) {
    SquarePtr new_square = malloc(sizeof(Square));
    if (new_square == NULL)
        return NULL;
    else {
        new_square->center = center;
        new_square->r = r;
        return new_square;
    }
}


void Square_destroy(SquarePtr *square) {
    Point_destroy(&(*square)->center);
    free(*square);
    *square = NULL;
}

int Square_isInside(void *sq, PointPtr point) {
    SquarePtr square = sq;
    float centerX = Point_getX(square->center);
    float centerY = Point_getY(square->center);
    float pointX = Point_getX(point);
    float pointY = Point_getY(point);
    return abs(pointX - centerX) + abs(pointY - centerY) <= square->r;
}
