#include "ring.h"

typedef struct Ring {
    float r1_squared;
    float r2_squared;
    PointPtr center;
} Ring;


RingPtr Ring_create(PointPtr center, float r1, float r2) {
    RingPtr new_ring = malloc(sizeof(Ring));
    if (new_ring == NULL)
        return NULL;
    else {
        new_ring->center = center;
        new_ring->r1_squared = r1 * r1;
        new_ring->r2_squared = r2 * r2;
        return new_ring;
    }
}


void Ring_destroy(RingPtr *ring) {
    Point_destroy(&(*ring)->center);
    free(*ring);
    *ring = NULL;
}


int Ring_isInside(void *rng, PointPtr point) {
    /*
     * r2^2 <= (pointX - centerX)^2 + (pointY - centerY)^2 <= r1^2
     */
    RingPtr ring = rng;
    float centerX = Point_getX(ring->center), centerY = Point_getY(ring->center);
    float pointX = Point_getX(point), pointY = Point_getY(point);
    float rg = pow(pointX - centerX, 2.0) + pow(pointY - centerY, 2.0);
    return (ring->r2_squared <= rg && rg <= ring->r1_squared);
}
