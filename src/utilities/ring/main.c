#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ring.h"


#define ARGS_ERROR 1
#define FILE_ERROR 2

int main(int argc, char *argv[])
{
    FILE *input_binary_file = NULL, *output_file = NULL;
    int i, offset = 0, points_to_read_count = 0;
    float x = 0.0, y = 0.0, r1 = 0.0, r2 = 0.0;

    /*
     * Read arguments
     */
    if (argc < 9) {
        printf("usage: %s -i InputBinaryFile -o OutputFile -a "\
                "x y r1 r2 [-f Offset] [-n PointsToReadCount]\n", argv[0]);
        exit(ARGS_ERROR);
    }

    for (i = 1; i < argc - 1; i++)
    {
        if (strcmp(argv[i], "-i") == 0) {
            input_binary_file = fopen(argv[i + 1], "rb");
            if (input_binary_file == NULL) {
                if (output_file != NULL)
                    fclose(output_file);
                perror("input binary file");
                exit(FILE_ERROR);
            }
        }
        else if (strcmp(argv[i], "-o") == 0) {
            output_file = fopen(argv[i + 1], "w");
            if (output_file == NULL) {
                if (input_binary_file != NULL)
                    fclose(input_binary_file);
                perror("output file");
                exit(FILE_ERROR);
            }
        }
        else if (strcmp(argv[i], "-a") == 0) {
            if (i + 4 < argc) {
                x = atof(argv[i + 1]);
                y = atof(argv[i + 2]);
                r1 = atof(argv[i + 3]);
                r2 = atof(argv[i + 4]);
            }
        }
        else if (strcmp(argv[i], "-f") == 0)
            offset = atoi(argv[i + 1]);
        else if (strcmp(argv[i], "-n") == 0)
            points_to_read_count = atoi(argv[i + 1]);
    }

    if (input_binary_file == NULL || output_file == NULL) {
        printf("usage: %s -i InputBinaryFile -o OutputFile -a "\
                "x y r1 r2 [-f Offset] [-n PointsToReadCount]\n", argv[0]);
        exit(ARGS_ERROR);
    }

    PointPtr ring_center = Point_create(x, y);
    RingPtr ring = Ring_create(ring_center, r1, r2);

    checkPoints(ring, offset, points_to_read_count,
                            input_binary_file, output_file, &Ring_isInside);
    Ring_destroy(&ring);

    fclose(input_binary_file);
    fclose(output_file);
    exit(EXIT_SUCCESS);
}
