#ifndef _RING_H
#define _RING_H

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "../point.h"

typedef struct Ring* RingPtr;

RingPtr Ring_create(PointPtr center, float r1, float r2);

void Ring_destroy(RingPtr *ring);

int Ring_isInside(void *rng, PointPtr point);

#endif
