#ifndef _ELLIPSE_H
#define _ELLIPSE_H

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "../point.h"

typedef struct Ellipse* EllipsePtr;

EllipsePtr Ellipse_create(float h, float k, float a, float b);

void Ellipse_destroy(EllipsePtr *ellipse);

int Ellipse_isInside(void *ellips, PointPtr point);

#endif
