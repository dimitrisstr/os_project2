#include "ellipse.h"

typedef struct Ellipse {
    float h;
    float k;
    float a_squared;
    float b_squared;
} Ellipse;

EllipsePtr Ellipse_create(float h, float k, float a, float b) {
    EllipsePtr new_ellipse = malloc(sizeof(Ellipse));
    if (new_ellipse == NULL)
        return NULL;
    else {
        new_ellipse->h = h;
        new_ellipse->k = k;
        new_ellipse->a_squared = a * a;
        new_ellipse->b_squared = b * b;
        return new_ellipse;
    }
}

void Ellipse_destroy(EllipsePtr *ellipse) {
    free(*ellipse);
    *ellipse = NULL;
}
int Ellipse_isInside(void *ellips, PointPtr point) {
    /*
     * ((x - h)^2 / a^2) + ((y - k)^2 / b^2) <= 1
     */
    EllipsePtr ellipse = ellips;
    float pointX = Point_getX(point), pointY = Point_getY(point);
    return ( (pow(pointX - ellipse->h, 2.0) / ellipse->a_squared) +
            (pow(pointY - ellipse->k, 2.0) / ellipse->b_squared) ) <= 1;
}
