#include "point.h"

typedef struct Point {
    float x;
    float y;
} Point;


PointPtr Point_create(float x, float y) {
    PointPtr new_point = malloc(sizeof(Point));
    if (new_point == NULL)
        return NULL;
    else {
        new_point->x = x;
        new_point->y = y;
        return new_point;
    }
}


void Point_destroy(PointPtr *point) {
    free(*point);
    *point = NULL;
}


float Point_getX(PointPtr point) {
    return point->x;
}


float Point_getY(PointPtr point) {
    return point->y;
}


void Point_read(PointPtr point, FILE *input_file) {
    fread(point, sizeof(Point), 1, input_file);
}


void Point_print(PointPtr point, FILE *output_file) {    
    fprintf(output_file, "%.2f\t%.2f\n", point->x, point->y);    
}

void checkPoints(void *shape, int offset, int points_to_read_count,
    FILE *input_file, FILE *output_file, int (*isInside)(void *, PointPtr point)) {
    //find file's size
    fseek(input_file, 0, SEEK_END);
    int file_size = ftell(input_file);
    rewind(input_file);
    //find number of records remaining
    fseek(input_file, offset * sizeof(Point), SEEK_SET);
    int size = ftell(input_file);
    int records_remaining = (file_size - size) / sizeof(Point);

    if (points_to_read_count == 0 || points_to_read_count > records_remaining)
        points_to_read_count = records_remaining;

    Point new_point;
    int i;
    for (i = 0; i < points_to_read_count; i++) {
        Point_read(&new_point, input_file);
        if ((*isInside)(shape, &new_point))
            Point_print(&new_point, output_file);
    }
}
