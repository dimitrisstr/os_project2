#include "semicircle.h"

typedef struct Semicircle {
    PointPtr center;
    float r_squared;
} Semicircle;


SemicirclePtr Semicircle_create(PointPtr center, int r) {
    SemicirclePtr new_semicircle = malloc(sizeof(Semicircle));
    if (new_semicircle == NULL)
        return NULL;
    else {
        new_semicircle->center = center;
        new_semicircle->r_squared = r * r;
        return new_semicircle;
    }
}


void Semicircle_destroy(SemicirclePtr *semicircle) {
    Point_destroy(&(*semicircle)->center);
    free(*semicircle);
    *semicircle = NULL;
}


int Semicircle_isInsideNorth(void *semicirc, PointPtr point) {
    SemicirclePtr semicircle = semicirc;
    float centerX = Point_getX(semicircle->center);
    float centerY = Point_getY(semicircle->center);
    float pointX = Point_getX(point);
    float pointY = Point_getY(point);
    float circle = pow(pointX - centerX, 2.0) + pow(pointY - centerY, 2.0);
    if (circle <= semicircle->r_squared && pointY >= centerY)
        return 1;
    else
        return 0;
}


int Semicircle_isInsideSouth(void *semicirc, PointPtr point) {
    SemicirclePtr semicircle = semicirc;
    float centerX = Point_getX(semicircle->center);
    float centerY = Point_getY(semicircle->center);
    float pointX = Point_getX(point);
    float pointY = Point_getY(point);
    float circle = pow(pointX - centerX, 2.0) + pow(pointY - centerY, 2.0);
    if (circle <= semicircle->r_squared && pointY <= centerY)
        return 1;
    else
        return 0;
}


int Semicircle_isInsideWest(void *semicirc, PointPtr point) {
    SemicirclePtr semicircle = semicirc;
    float centerX = Point_getX(semicircle->center);
    float centerY = Point_getY(semicircle->center);
    float pointX = Point_getX(point);
    float pointY = Point_getY(point);
    float circle = pow(pointX - centerX, 2.0) + pow(pointY - centerY, 2.0);
    if (circle <= semicircle->r_squared && pointX <= centerX)
        return 1;
    else
        return 0;
}


int Semicircle_isInsideEast(void *semicirc, PointPtr point) {
    SemicirclePtr semicircle = semicirc;
    float centerX = Point_getX(semicircle->center);
    float centerY = Point_getY(semicircle->center);
    float pointX = Point_getX(point);
    float pointY = Point_getY(point);
    float circle = pow(pointX - centerX, 2.0) + pow(pointY - centerY, 2.0);
    if (circle <= semicircle->r_squared && pointX >= centerX)
        return 1;
    else
        return 0;
}
