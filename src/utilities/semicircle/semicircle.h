#ifndef _SEMICIRCLE_H
#define _SEMICIRCLE_H

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "../point.h"

typedef struct Semicircle* SemicirclePtr;

SemicirclePtr Semicircle_create(PointPtr center, int r);

void Semicircle_destroy(SemicirclePtr *semicircle);

int Semicircle_isInside(void *semicirc, PointPtr point);

int Semicircle_isInsideNorth(void *semicirc, PointPtr point);

int Semicircle_isInsideSouth(void *semicirc, PointPtr point);

int Semicircle_isInsideWest(void *semicirc, PointPtr point);

int Semicircle_isInsideEast(void *semicirc, PointPtr point);

#endif
