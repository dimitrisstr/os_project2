#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "semicircle.h"


#define ARGS_ERROR 1
#define FILE_ERROR 2

int main(int argc, char *argv[])
{
    FILE *input_binary_file = NULL, *output_file = NULL;
    int i, offset = 0, points_to_read_count = 0;
    float x = 0.0, y = 0.0, r = 0.0;
    char position = 'U';

    /*
     * Read arguments
     */
    if (argc < 9) {
        printf("usage: %s -i InputBinaryFile -o OutputFile -a "\
                "x y r N/S/W/E [-f Offset] [-n PointsToReadCount]\n", argv[0]);
        exit(ARGS_ERROR);
    }

    for (i = 1; i < argc - 1; i++)
    {
        if (strcmp(argv[i], "-i") == 0) {
            input_binary_file = fopen(argv[i + 1], "rb");
            if (input_binary_file == NULL) {
                if (output_file != NULL)
                    fclose(output_file);
                perror("input binary file");
                exit(FILE_ERROR);
            }
        }
        else if (strcmp(argv[i], "-o") == 0) {
            output_file = fopen(argv[i + 1], "w");
            if (output_file == NULL) {
                if (input_binary_file != NULL)
                    fclose(input_binary_file);
                perror("output file");
                exit(FILE_ERROR);
            }
        }
        else if (strcmp(argv[i], "-a") == 0) {
            if (i + 4 < argc) {
                x = atof(argv[i + 1]);
                y = atof(argv[i + 2]);
                r = atof(argv[i + 3]);
                position = argv[i + 4][0];
                /*
                 * Valid Postions
                 * N: North
                 * S: South
                 * W: West
                 * E: East
                 */
                if (position != 'N' && position != 'S' &&
                                    position != 'E' && position != 'W')
                    position = 'U';
            }
        }
        else if (strcmp(argv[i], "-f") == 0)
            offset = atoi(argv[i + 1]);
        else if (strcmp(argv[i], "-n") == 0)
            points_to_read_count = atoi(argv[i + 1]);
    }

    if (position == 'U' || input_binary_file == NULL || output_file == NULL) {
        if (input_binary_file != NULL)
            fclose(input_binary_file);
        if (output_file != NULL)
            fclose(output_file);
        printf("usage: %s -i InputBinaryFile -o OutputFile -a "\
        "x y r N/S/W/E [-f Offset] [-n PointsToReadCount]\n", argv[0]);
        exit(ARGS_ERROR);
    }

    PointPtr semicircle_center = Point_create(x, y);
    SemicirclePtr semicircle = Semicircle_create(semicircle_center, r);
    int (*isInside)(void*, PointPtr);
    if (position == 'N')
        isInside = &Semicircle_isInsideNorth;
    else if (position == 'S')
        isInside = &Semicircle_isInsideSouth;
    else if (position == 'W')
        isInside = &Semicircle_isInsideWest;
    else
        isInside = &Semicircle_isInsideEast;
        
    checkPoints(semicircle, offset, points_to_read_count,
                    input_binary_file, output_file, isInside);
    Semicircle_destroy(&semicircle);

    fclose(input_binary_file);
    fclose(output_file);
    exit(EXIT_SUCCESS);
}
