#include "script.h"


int createScript(char *temp_directory, int *handlers_pids, char **handler_colors,
                  int handlers_count) {
    static int command_count = 1;
    char *file_name = emalloc(sizeof(char) * (strlen(temp_directory) + 64));
    sprintf(file_name, "%d_script.gnuplot", command_count); //script file name
    FILE *script_file = fopen(file_name, "w");
    if (script_file == NULL) {
        perror("script");
        free(file_name);
        return FAILED;
    }

    fprintf(script_file, "set terminal png\n"\
                         "set size ratio -1\n"\
                         "set output \"./%d_image.png\"\n"\
                         "plot \\\n", command_count);
    int i;
    for (i = 0; i < handlers_count - 1; i++)
        fprintf(script_file, "\"%s/%d.out\" notitle with points "\
                         "pointsize 0.5 linecolor rgb \"%s\",\\\n",
                          temp_directory, handlers_pids[i], handler_colors[i]);

    fprintf(script_file, "\"%s/%d.out\" notitle with points "\
                         "pointsize 0.5 linecolor rgb \"%s\"\n",
                          temp_directory, handlers_pids[i], handler_colors[i]);
    fflush(script_file);
    fclose(script_file);

    //execute gnuplot script
    int pid = fork();
    if (pid == 0) {
        execlp("gnuplot", "gnuplot", file_name, NULL);
        exit(EXIT_FAILURE);
    } else
        wait(NULL);

    //remove script file
    remove(file_name);
    //remove handler output files
    for (i = 0; i < handlers_count; i++) {
        sprintf(file_name, "%s/%d.out", temp_directory, handlers_pids[i]);
        remove(file_name);
    }
    free(file_name);
    command_count++;
    return SUCCESS;
}
