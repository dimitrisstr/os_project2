#ifndef _SCRIPT_H
#define _SCRIPT_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include "malloc.h"

#define SUCCESS 0
#define FAILED 1

int createScript(char *temp_directory, int *handlers_pids, char **handler_colors,
                  int handlers_count);

#endif
