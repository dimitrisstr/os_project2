#include "handler.h"


typedef struct Handler {
    int **workers_data;             //workers array
    int *workers_pids;              //workers pids array
    int *pipes_fds;                 //pipes file descriptors
    int workers_count;              //number of workers
    char *temp_directory;           //directory to create pipes
    char **worker_pipes;            //worker pipes names
    char *args_vector[MAX_ARGS];    //arguments vector for the exec
} Handler;

enum Args {
    utility_name_arg,
    input_file_flag,
    input_file_arg,
    output_file_flag,
    output_file_arg,
    offset_flag,
    offset_arg,
    points_to_read_flag,
    points_to_read_arg,
    utility_args_flag
};


void Handler_read(int input_file, int output_file);


HandlerPtr Handler_create(char *utility_path, int workers_count, int **workers_data,
                   char *directory, char *input_file, char **args) {

    HandlerPtr new_handler = emalloc(sizeof(Handler));
    Handler_createArgsVector(new_handler, utility_path, args, input_file);

    new_handler->workers_count = workers_count;
    new_handler->workers_data = workers_data;
    //temp directory to create pipes
    new_handler->temp_directory = emalloc(sizeof(char) * (strlen(directory) + 1));
    strcpy(new_handler->temp_directory, directory);
    //array to save workers pids
    new_handler->workers_pids = emalloc(sizeof(int) * workers_count);
    //array to save pipe names
    new_handler->worker_pipes = emalloc(sizeof(char*) * workers_count);
    new_handler->pipes_fds = emalloc(sizeof(int) * workers_count);

    return new_handler;
}

int Handler_createArgsVector(HandlerPtr handler, char *utility_path, char **args,
                                                                char *input_file) {

    /*
     * ./utilityX -i InputBinaryFile -o OutputFile -f Offset
     *                      -n PointsToReadCount -a UtilityArgs
     */
    handler->args_vector[utility_name_arg] =
                        emalloc(sizeof(char) * (strlen(utility_path) + 1));
    strcpy(handler->args_vector[utility_name_arg], utility_path);
    handler->args_vector[input_file_arg] =
                        emalloc(sizeof(char) * (strlen(input_file) + 1));
    strcpy(handler->args_vector[input_file_arg], input_file);

    int i, args_index = 0;
    for (i = utility_args_flag + 1; args[args_index] != NULL; i++, args_index++) {
        handler->args_vector[i] =
                emalloc(sizeof(char) * (strlen(args[args_index]) + 1));
        strcpy(handler->args_vector[i], args[args_index]);
    }

    handler->args_vector[i] = NULL;
    free(args);
    /* args vector flags */
    handler->args_vector[input_file_flag] = "-i";
    handler->args_vector[output_file_flag] = "-o";
    handler->args_vector[offset_flag] = "-f";
    handler->args_vector[points_to_read_flag] = "-n";
    handler->args_vector[utility_args_flag] = "-a";

    return SUCCESS;
}

void Handler_createPipes(HandlerPtr handler) {
    int i;
    for (i = 0; i < handler->workers_count; i++) {
        handler->worker_pipes[i] =
                emalloc(sizeof(char) * (strlen(handler->temp_directory) + 64));
        sprintf(handler->worker_pipes[i], "%s/%d_w%d.fifo",
                handler->temp_directory, getpid(), i);

        if (mkfifo(handler->worker_pipes[i], FIFO_PERMS) == -1 && errno != EEXIST) {
            perror("pipe");
            exit(EXIT_FAILURE);
        }
    }
}


void Handler_createWorkers(HandlerPtr handler) {
    handler->args_vector[offset_arg] = emalloc(sizeof(char) * 32);
    handler->args_vector[points_to_read_arg] = emalloc(sizeof(char) * 32);

    int i;
    for (i = 0; i < handler->workers_count; i++) {
        handler->workers_pids[i] = fork();
        if (handler->workers_pids[i] == 0) {
            handler->args_vector[output_file_arg] = handler->worker_pipes[i];

            sprintf(handler->args_vector[offset_arg], "%d",
                                           handler->workers_data[OFFSET][i]);
            sprintf(handler->args_vector[points_to_read_arg], "%d",
                                           handler->workers_data[POINTS_TO_READ][i]);

            execv(handler->args_vector[0], handler->args_vector);
            exit(EXIT_FAILURE);
        }
        else if (handler->workers_pids[i] > 0)
            handler->pipes_fds[i] = open(handler->worker_pipes[i], O_RDONLY | O_NONBLOCK);
        else
            exit(EXIT_FAILURE);
    }
}

void Handler_readPipes(HandlerPtr handler) {
    int i, worker_index, pipe_fd, output_file_fd, worker_pid;
    char *output_file =
            emalloc(sizeof(char) * (strlen(handler->temp_directory) + 15));

    sprintf(output_file, "%s/%d.out", handler->temp_directory, getpid());
    output_file_fd = open(output_file, O_WRONLY | O_CREAT, OUTPUT_FILE_PERMS);

    for (i = 0; i < handler->workers_count; i++) {
        worker_pid = wait(NULL);
        //find worker index
        for (worker_index = 0; worker_index < handler->workers_count; worker_index++)
            if (handler->workers_pids[worker_index] == worker_pid)
                break;
        pipe_fd = handler->pipes_fds[worker_index];
        Handler_read(pipe_fd, output_file_fd);
        close(pipe_fd);
    }
    free(output_file);
    close(output_file_fd);
}

void Handler_read(int input_file, int output_file) {
    int num_read;
    char buffer[BUFFER_SIZE];
    while ((num_read = read(input_file, buffer, BUFFER_SIZE)) > 0) {
        if (write(output_file, buffer, num_read) < num_read) {
            perror("write");
            exit(EXIT_FAILURE);
        }
    }
}


void Handler_removePipes(HandlerPtr handler) {
    int i;
    for (i = 0; i < handler->workers_count; i++) {
        remove(handler->worker_pipes[i]);
        free(handler->worker_pipes[i]);
    }
}

void Handler_destroy(HandlerPtr *handler) {

    free((*handler)->args_vector[utility_name_arg]);
    free((*handler)->args_vector[input_file_arg]);
    free((*handler)->args_vector[offset_arg]);
    free((*handler)->args_vector[points_to_read_arg]);

    int i;
    for (i = utility_args_flag + 1; (*handler)->args_vector[i] != NULL; i++)
        free((*handler)->args_vector[i]);

    free((*handler)->workers_data[0]);
    free((*handler)->workers_data[1]);
    free((*handler)->workers_data);
    free((*handler)->workers_pids);
    free((*handler)->temp_directory);
    free((*handler)->worker_pipes);
    free((*handler)->pipes_fds);
    free(*handler);
    *handler = NULL;
}
