#ifndef _HANDLER_H
#define _HANDLER_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <ctype.h>
#include <errno.h>
#include "malloc.h"

#define FIFO_PERMS 0666
#define OUTPUT_FILE_PERMS 0600

#define BUFFER_SIZE 256
#define MAX_ARGS 15

#define OFFSET 0
#define POINTS_TO_READ 1

#define SUCCESS 0
#define FAILED 1

typedef struct Handler* HandlerPtr;

HandlerPtr Handler_create(char *utility_path, int workers_count, int **workers_data,
                      char *directory, char *input_file, char **args);

int Handler_createArgsVector(HandlerPtr handler, char *utility_path, char **args,
                             char *input_file);

void Handler_createPipes(HandlerPtr handler);

void Handler_createWorkers(HandlerPtr handler);

void Handler_readPipes(HandlerPtr handler);

void Handler_removePipes(HandlerPtr handler);

void Handler_destroy(HandlerPtr *handler);

#endif
