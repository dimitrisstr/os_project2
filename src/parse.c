#include "parse.h"


enum Utilities {
    circle,
    ellipse,
    ring,
    semicircle,
    square,
    unknown
};


static int utilities_args_count[] = {
    3,  //circle
    4,  //ellipse
    4,  //ring
    4,  //semicircle
    3   //square
};


static char *utilities_paths[] = {
        "utilities/circle/circle",
        "utilities/ellipse/ellipse",
        "utilities/ring/ring",
        "utilities/semicircle/semicircle",
        "utilities/square/square"
};


void startCLI(char *input_file, int **workers_data, int workers_count
                                                , char *temp_directory) {
    char buffer[COMMAND_BUFFER_SIZE];
    printf("> ");
    while (fgets(buffer, COMMAND_BUFFER_SIZE - 1, stdin) != NULL) {
        parseCommands(buffer, input_file, workers_data, workers_count,
                                                temp_directory);
        printf("> ");
    }
}


int parseCommands(char *buffer, char *input_file, int **workers_data,
                               int workers_count, char *temp_directory) {
    char *saveptr, *commands = strtok_r(buffer, "\n", &saveptr);
    if (commands == NULL)
        return FAILED;
    else if (strcmp(commands, "exit") == 0) {
        free(workers_data[0]);
        free(workers_data[1]);
        free(workers_data);
        exit(EXIT_SUCCESS);
    }
    else {
        /*
         * command format:
         * shape1 args color, shapes2 args color, ... ;
         */
        if (commands[strlen(commands) -1] != ';') {
            printf("Invalid command!\n");
            return FAILED;
        }
        int i, commands_count = 0;
        for (i = 0; i < strlen(commands); i++)
            if (commands[i] == ',' || commands[i] == ';')
                commands_count++;

        int handler_index = 0, error = 0;
        int *handler_pids = emalloc(sizeof(int) * commands_count);
        char **handler_colors = emalloc(sizeof(char*) * commands_count);

        char *command_saveptr, *utility_path;
        char **args = emalloc(sizeof(char*) * MAX_UTILITY_ARGS);

        char *command_token = strtok_r(commands, ",;", &command_saveptr);
        for (i = 0; i < commands_count; i++) {
            error = parseArgs(command_token, &utility_path, args,
                                             &handler_colors[handler_index]);
            if (error == SUCCESS) {
                handler_pids[handler_index] = fork();
                if (handler_pids[handler_index] == 0) { //create a new handler
                    free(handler_colors);
                    free(handler_pids);
                    HandlerPtr handler = Handler_create(utility_path,
                     workers_count, workers_data, temp_directory, input_file, args);

                    Handler_createPipes(handler);
                    Handler_createWorkers(handler);
                    Handler_readPipes(handler);
                    Handler_removePipes(handler);
                    Handler_destroy(&handler);
                    exit(EXIT_SUCCESS);
                }
                else if (handler_pids[handler_index] > 0)
                    handler_index++;
                else
                    exit(EXIT_FAILURE);
            }
            else
                printError(error);
            command_token = strtok_r(NULL, ",;", &command_saveptr);
        }
        free(args);
        for (i = 0; i < handler_index; i++)
            waitpid(handler_pids[i], NULL, 0);
        if (handler_index > 0)
            createScript(temp_directory, handler_pids, handler_colors, handler_index);
        free(handler_colors);
        free(handler_pids);
        return SUCCESS;
    }
}


int parseArgs(char *command, char **utility_path,
                                    char **utility_args, char **color) {
    char *saveptr, *token;
    int args_count = countArgs(command) - 2;    //number of args without utility and color
    token = strtok_r(command, " \t", &saveptr);
    enum Utilities utility = findUtility(token);

    if (utility == unknown)
        return UNKNOWN_UTILITY;
    if (utilities_args_count[utility] != args_count)
        return WRONG_ARGS;
    else {
        *utility_path = utilities_paths[utility];
        int i;
        for (i = 0; i < args_count; i++)
           utility_args[i] = strtok_r(NULL, " \t", &saveptr);

        utility_args[i] = NULL;
        *color = strtok_r(NULL, " \t", &saveptr);
        return SUCCESS;
    }
}


int countArgs(char *command) {
    int i, args_count = 0;
    for (i = 0; i < strlen(command); i++)
        if ((command[i] != ' ' && command[i] != '\t') && (command[i + 1] == ' '
                          || command[i + 1] == '\t' || command[i + 1] == '\0'))
            args_count++;
    return args_count;
}


enum Utilities findUtility(char *utility_name) {
    if (strcmp(utility_name, "circle") == 0)
        return circle;
    else if (strcmp(utility_name, "square") == 0)
        return square;
    else if (strcmp(utility_name, "semicircle") == 0)
        return semicircle;
    else if (strcmp(utility_name, "ellipse") == 0)
        return ellipse;
    else if (strcmp(utility_name, "ring") == 0)
        return ring;
    else
        return unknown;
}


void printError(int error) {
    switch (error) {
        case WRONG_ARGS:
            fprintf(stderr, "Wrong arguments!\n");
            break;
        case UNKNOWN_UTILITY:
            fprintf(stderr, "Uknown utility!\n");
            break;
        default:
            fprintf(stderr, "Error!\n");
    }
}
