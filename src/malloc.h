#ifndef _MALLOC_H
#define _MALLOC_H

#include <stdio.h>
#include <stdlib.h>

void* emalloc(int size);

#endif
