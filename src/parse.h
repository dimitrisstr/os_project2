#ifndef _PARSE_H
#define _PARSE_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "malloc.h"
#include "handler.h"
#include "script.h"

#define SUCCESS 0
#define FAILED 1
#define UNKNOWN_UTILITY 2
#define WRONG_ARGS 3
#define COMMAND_BUFFER_SIZE 1024
#define MAX_UTILITY_ARGS 5


void startCLI(char *input_file, int **workers_data, int workers_count,
                                                     char *temp_directory);

int parseCommands(char *buffer, char *input_file, int **workers_data,
                  int workers_count, char *temp_directory);

void printError(int error);

int parseArgs(char *command, char **utility_path,
              char **utility_args, char **color);

int countArgs(char *command);

enum Utilities findUtility(char *utility_name);

#endif
