CC = gcc
OUT = shapes
BIN_DIR = bin
SRC_DIR = src
OBJS = malloc.o main.o handler.o parse.o script.o
OBJS_DIR = $(addprefix $(BIN_DIR)/,$(OBJS))
FLAGS = -Wall -O2 -c -o $@

all: $(OBJS_DIR)
	$(CC) -o $(BIN_DIR)/$(OUT) $(OBJS_DIR)
	$(MAKE) -C $(SRC_DIR)/utilities/circle
	$(MAKE) -C $(SRC_DIR)/utilities/square
	$(MAKE) -C $(SRC_DIR)/utilities/ellipse
	$(MAKE) -C $(SRC_DIR)/utilities/semicircle
	$(MAKE) -C $(SRC_DIR)/utilities/ring

$(BIN_DIR)/main.o: $(SRC_DIR)/main.c
	$(CC) $(FLAGS) $<
	
$(BIN_DIR)/malloc.o: $(SRC_DIR)/malloc.c $(SRC_DIR)/malloc.h
	$(CC) $(FLAGS) $<
	
$(BIN_DIR)/handler.o: $(SRC_DIR)/handler.c $(SRC_DIR)/handler.h
	$(CC) $(FLAGS) $<
	
$(BIN_DIR)/parse.o: $(SRC_DIR)/parse.c $(SRC_DIR)/parse.h
	$(CC) $(FLAGS) $<
	
$(BIN_DIR)/script.o: $(SRC_DIR)/script.c $(SRC_DIR)/script.h
	$(CC) $(FLAGS) $<
	
$(OBJS_DIR): | $(BIN_DIR)

$(BIN_DIR):
	mkdir -p $(BIN_DIR)
	
clean:
	rm -f $(BIN_DIR)/$(OUT) $(OBJS_DIR)
	$(MAKE) clean -C $(SRC_DIR)/utilities/circle
	$(MAKE) clean -C $(SRC_DIR)/utilities/square
	$(MAKE) clean -C $(SRC_DIR)/utilities/ellipse
	$(MAKE) clean -C $(SRC_DIR)/utilities/semicircle
	$(MAKE) clean -C $(SRC_DIR)/utilities/ring	
